//
//  CreateStopViewController.swift
//  TravelApp
//
//  Created by User on 9.02.21.
//

import UIKit
import MapKit
import FirebaseDatabase
import RealmSwift


class CreateStopViewController: UIViewController, SpentMoneyViewControllerDelegate, MapViewControllerDelegate {

    @IBOutlet weak var nameCityTextField: UITextField!
    
    @IBOutlet weak var labelRating: UILabel!
    
    @IBOutlet weak var descriptionTextView: UITextView!
    
    @IBOutlet weak var lantitudeLabel: UILabel!
    
    @IBOutlet weak var longitudeLabel: UILabel!
    
    
    @IBOutlet weak var spentMoneyLabel: UILabel!
    @IBOutlet weak var transportSegmentedControl: UISegmentedControl!
    
   
    var stop: RealmStop!
    var stops: Results<RealmStop>!
    var travel: RealmTravel!
    var transport: Transport?
    var currencyCreate: Currency?
    var latitude: Double?
    var longitude: Double?
    var stopNotificationToken: NotificationToken!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelRating.text = "0"
        transport = .plane
        
        if let stop = stop {
            nameCityTextField.text = stop.name
            labelRating.text = "\(stop.rating)"
            spentMoneyLabel.text = "\(stop.spentMoney)"
            lantitudeLabel.text = String(format: "%.2f", stop.latitude)
            longitudeLabel.text = String(format: "%.2f", stop.longitude)
            descriptionTextView.text = stop.desc
            if stop.transport == .plane {
                transportSegmentedControl.selectedSegmentIndex = 0
            } else if stop.transport == .train {
                transportSegmentedControl.selectedSegmentIndex = 1
            } else if stop.transport == .car {
                transportSegmentedControl.selectedSegmentIndex = 2
            }
            transport = stop.transport
            currencyCreate = Currency(rawValue: stop.spentMoneyCurrencyString)!
            latitude = stop.latitude
            longitude = stop.longitude
        }
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.isNavigationBarHidden = false
        navigationItem.title = "Stop"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector( saveStopClicked(_:)))
        navigationItem.rightBarButtonItem?.tintColor = UIColor(named: "ColorForBar")
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(cancelButtonClicked(_:)))
        navigationItem.leftBarButtonItem?.tintColor = UIColor(named: "ColorForBar")
        
    }
    
    @objc func cancelButtonClicked(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func spentMoneySaveClicked(_ text: String, currency: Currency) {
        spentMoneyLabel.text = text
        currencyCreate = currency
    }

    @objc func saveStopClicked(_ sender: UIButton) {
        let realm = try! Realm()
        
        if let stop = stop {
            try! realm.write {
                
                stop.name = nameCityTextField.text!
                guard let rating = Int(labelRating.text!),
                      let money = Double(spentMoneyLabel.text!),
                      let currency = currencyCreate,
                      let transport = transport,
                      let latitude = latitude,
                      let longitude = longitude else { return }
                stop.rating = rating
                stop.desc = descriptionTextView.text
                stop.spentMoney = money
                stop.spentMoneyCurrencyString = currency.rawValue
                stop.transport = transport
                stop.latitude = latitude
                stop.longitude = longitude
            }
        } else {
            stop = RealmStop()
            stop.id = UUID().uuidString
            stop.travelId = travel.id
            guard let name = nameCityTextField.text,
                  let rating = Int(labelRating.text!),
                  let money = Double(spentMoneyLabel.text!),
                  let currency = currencyCreate,
                  let transport = transport,
                  let latitude = latitude,
                  let longitude = longitude else { return }
            stop.name = name
            stop.rating = rating
            stop.desc = descriptionTextView.text
            stop.spentMoney = money
            stop.spentMoneyCurrencyString = currency.rawValue
            stop.transport = transport
            stop.latitude = latitude
            stop.longitude = longitude
        
            try! realm.write {
                realm.add(stop!, update: .all)
            }
            let realmTravel = try! Realm()
            
                try! realmTravel.write {
                    travel.stops.append(stop)
                }
        }
        let json : [String:Any] = ["id" : stop!.id,
                                   "travelId": stop!.travelId,
                                   "rating": stop!.rating ,
                                   "name": stop!.name,
                                   "spentMoney": stop!.spentMoney,
                                   "latitude" :  stop!.latitude,
                                   "longitude" : stop!.longitude,
                                   "transport" : stop!.transport.rawValue,
                                   "currency": stop!.spentMoneyCurrencyString,
                                   "description" : stop!.desc]
        
        let dataBase = Database.database().reference()
        let child = dataBase.child("stops").child("\(stop!.id)")
        child.setValue(json) { (error, ref) in
            
        }
        

        navigationController?.popViewController(animated: true)
    }
    

    @IBAction func segmentedControlClicked(_ sender: Any) {
        if transportSegmentedControl.selectedSegmentIndex == 0  {
            transport = .plane
        } else if transportSegmentedControl.selectedSegmentIndex == 1 {
            transport = .train
        } else if transportSegmentedControl.selectedSegmentIndex == 2 {
            transport = .car
        }
    }
    
    @IBAction func chooseCurrencyButton(_ sender: UIButton) {
        let spentMoneyVC = SpentMoneyViewController()
        spentMoneyVC.delegateCreateStop = self
        present(spentMoneyVC, animated: true, completion: nil)
    }
    
    @IBAction func mapButtonClicked(_ sender: UIButton) {
        let mapVC = MapViewController()
        mapVC.delegate = self
        mapVC.latitude = latitude
        mapVC.longitude = longitude
        navigationController?.pushViewController(mapVC, animated: true)
    }
    
    @IBAction func minusRatingButtonClicked(_ sender: UIButton) {
        if labelRating.text == "0" {
            labelRating.text = String(Int(labelRating.text!)! - 0)
        } else {
            labelRating.text = String(Int(labelRating.text!)! - 1)
        }
    }
    
    @IBAction func plusRatingButtonClicked(_ sender: UIButton) {
        if labelRating.text == "5" {
            labelRating.text = String(Int(labelRating.text!)! + 0)
        } else {
            labelRating.text = String(Int(labelRating.text!)! + 1)
        }

    }
    
    func mapControllerSelectLocation(latitude: Double, longitude: Double) {
        lantitudeLabel.text = String(format: "%.2f", latitude)
        longitudeLabel.text = String(format: "%.2f", longitude)
        guard let latitude = Double(lantitudeLabel.text!),
              let longitude = Double(longitudeLabel.text!)
                  else  { return }
        self.latitude = latitude
        self.longitude = longitude
    }

}
