//
//  FogotPasswordViewController.swift
//  TravelApp
//
//  Created by User on 9.02.21.
//

import UIKit
import Firebase
class FogotPasswordViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func resetPasswordButtonClicked(_ sender: UIButton) {
        let auth = Auth.auth()
        if let email = emailTextField.text {
            auth.sendPasswordReset(withEmail: email) { [weak self] (error) in
                if let error = error {
                    let errorMassege = error.localizedDescription
                    let alert = UIAlertController(title: nil, message: errorMassege, preferredStyle: .alert)
                    let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    alert.addAction(action)
                    guard let self = self else { return }
                    self.present(alert, animated: true, completion: nil)
                }
                
                let alert = UIAlertController(title: "Success", message: "A password reset has been send!", preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alert.addAction(action)
                guard let self = self else { return }
                self.present(alert, animated: true, completion: nil)
            }

        }
        
    }
    
    
    @IBAction func backButtonClicked(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    

}
