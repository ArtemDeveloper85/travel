//
//  LoginViewController.swift
//  TravelApp
//
//  Created by User on 9.02.21.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {

    @IBOutlet weak var textFieldEmail: UITextField!
 
    @IBOutlet weak var separatorEmail: UIView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var labelPassword: UILabel!
    
    @IBOutlet weak var separatorPassword: UIView!
    @IBOutlet weak var textFieldPassword: UITextField!
    
    @IBOutlet weak var buttonImage: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }

    @IBAction func backButtonClicked(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func seePasswordClicked(_ sender: UIButton) {
        let imageHideButton = UIImage(named: "hide")
        let imageViewButton = UIImage(named: "View")
        buttonImage.setImage(imageHideButton, for: .normal)
        buttonImage.setImage(imageViewButton, for: .selected)
        
        if sender.isSelected == true {
        textFieldPassword.isSecureTextEntry.toggle()
            sender.isSelected = false
            
        } else {
            sender.isSelected = true
            textFieldPassword.isSecureTextEntry.toggle()
        }
    }
    
    @IBAction func loginClicked(_ sender: UIButton) {
        if let email = textFieldEmail.text, let password = textFieldPassword.text {
            Auth.auth().signIn(withEmail: email, password: password) {[weak self] (user, error) in
                if user != nil {
                    let travelVC = TravelListViewController()
                    guard let self = self else { return }
                    self.navigationController?.pushViewController(travelVC, animated: true)
                } else {
                    let errorMassege = error?.localizedDescription ?? "Error"
                    let alertVC = UIAlertController(title: nil, message: errorMassege, preferredStyle: .alert)
                    let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                    alertVC.addAction(action)
                    guard let self = self else { return }
                    self.present(alertVC, animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func fogotButtonClicked(_ sender: UIButton) {
        let fogotPasswordVC = FogotPasswordViewController()
        navigationController?.pushViewController(fogotPasswordVC, animated: true)
    }
    
}
