//
//  MapViewController.swift
//  TravelApp
//
//  Created by User on 9.02.21.
//

import UIKit
import MapKit
protocol MapViewControllerDelegate {
    func mapControllerSelectLocation(latitude: Double, longitude: Double)
}

class MapViewController: UIViewController {
   
    @IBOutlet weak var mapView: MKMapView!

    
    var latitude: Double?
    var longitude: Double?
    var delegate: MapViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        navigationItem.title = "Map of the travels"
        
        let longTapRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longTab(sender:)))
        mapView.addGestureRecognizer(longTapRecognizer)
        if let latitude = latitude, let longitude = longitude {
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            mapView.addAnnotation(annotation)
        }
    }
    
    @objc func longTab(sender: UIGestureRecognizer) {
        switch sender.state {
       
        case .possible:
            print("possible")
        case .began:
            print("began")
            mapView.removeAnnotations(mapView.annotations)
            
            let locationInView = sender.location(in: mapView)
            let locationOnMap = mapView.convert(locationInView, toCoordinateFrom: mapView)
            let annotation = MKPointAnnotation()
            annotation.coordinate = locationOnMap
            mapView.addAnnotation(annotation)
            delegate?.mapControllerSelectLocation(latitude: locationOnMap.latitude, longitude: locationOnMap.longitude)
            
        case .changed:
            print("changed")

        case .ended:
            print("ended")

        case .cancelled:
            print("cancelled")

        case .failed:
            print("failed")

        @unknown default:
            print("default")
        }
    }
}
