//
//  RegistrationViewController.swift
//  TravelApp
//
//  Created by User on 9.02.21.
//

import UIKit
import Firebase

class RegistrationViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.isNavigationBarHidden = true
    }

    @IBAction func backButtonClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func registrationButtonClicked(_ sender: UIButton) {
        if let email = emailTextField.text, let password = passwordTextField.text {
            Auth.auth().createUser(withEmail: email, password: password) { [weak self] (user, error) in
                if user != nil {
                    let travelVC = TravelListViewController()
                    guard let self = self else { return }
                    self.navigationController?.pushViewController(travelVC, animated: true)
                } else {
                    
                    let errorMassege = error?.localizedDescription ?? "Error"
                    let alertVC = UIAlertController(title: nil, message: errorMassege, preferredStyle: .alert)
                    let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                    alertVC.addAction(action)
                    guard let self = self else { return }
                    self.present(alertVC, animated: true, completion: nil)
                }
            }
        }
    }
}
