//
//  SpentMoneyViewController.swift
//  TravelApp
//
//  Created by User on 9.02.21.
//

import UIKit

protocol SpentMoneyViewControllerDelegate {
    func spentMoneySaveClicked(_ text: String, currency: Currency) 
}
class SpentMoneyViewController: UIViewController {
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var spentMoney: Double = 0
    var currency: Currency?
    
    var delegateCreateStop: SpentMoneyViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }

    @IBAction func readyButtonClicked(_ sender: UIButton) {
        if let text = textField.text {
            if segmentedControl.selectedSegmentIndex == 0 {
                currency = .dollar
            } else if segmentedControl.selectedSegmentIndex == 1 {
                currency = .euro
            } else if segmentedControl.selectedSegmentIndex == 2 {
                currency = .ruble
            }
            
            guard  let currency = self.currency else { return }
            delegateCreateStop?.spentMoneySaveClicked(text, currency: currency)
            dismiss(animated: true, completion: nil)
        }
    }
}
