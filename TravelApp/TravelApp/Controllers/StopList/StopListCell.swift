//
//  StopListCell.swift
//  HomeTaskForLesson23StopList
//
//  Created by User on 24.02.21.
//

import UIKit

class StopListCell: UITableViewCell {
  
    

    @IBOutlet weak var nameStopLabel: UILabel!
    @IBOutlet weak var discriptionLabel: UILabel!
    @IBOutlet weak var spentMoneyLabel: UILabel!
    @IBOutlet weak var transportImageView: UIImageView!
    
   
    @IBOutlet weak var firstRatingImageView: UIImageView!
    
    @IBOutlet weak var secondRatingImageView: UIImageView!
    
    @IBOutlet weak var ratingStackView: UIStackView!
    
    @IBOutlet weak var thirdRatingImageView: UIImageView!
    
    @IBOutlet weak var fouthRatingImageView: UIImageView!
    
    @IBOutlet weak var fifthRatingImageView: UIImageView!
    
   
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
    }
    
   
    
}
   
