//
//  StopListViewController.swift
//  TravelApp
//
//  Created by User on 9.02.21.
//

import UIKit
import FirebaseDatabase
import RealmSwift


class StopListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    @IBOutlet weak var tableView: UITableView!
    
    var travel: RealmTravel!
    var stops:Results<RealmStop>?
    var stopNotificationToken: NotificationToken? 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let realm = try! Realm()
        
        stops = realm.objects(RealmStop.self).filter("travelId == %@", travel!.id)
        
        stopNotificationToken = stops?.observe {[weak self] (changes) in
            switch changes {
            
            case .initial(_):
                break
            case .update(_,  let deletions,  let insertions,  let modifications):
                print("deletions: \(deletions), insertions:\(insertions),modifications: \(modifications)  ")
                guard let self = self else { return }
                self.tableView.reloadData()
                break
            case .error(_):
                break
            }
            
        }
        tableView.delegate = self
        tableView.dataSource = self

        let xib = UINib(nibName: "StopListCell", bundle: nil)
        tableView.register(xib, forCellReuseIdentifier: "StopListCell")
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addBarButtonClicked(_:)))
        navigationItem.rightBarButtonItem?.tintColor = UIColor(named: "ColorForBar")
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Travels", style: .done, target: self, action: #selector(backToTravelListController(_:)))
        navigationItem.leftBarButtonItem?.tintColor = UIColor(named: "ColorForBar")
        title = travel?.name
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let createStopVC = CreateStopViewController() 
       
        createStopVC.travel = self.travel
        createStopVC.stop = stops?[indexPath.row]
        navigationController?.pushViewController(createStopVC, animated: true)
    }
    
    
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stops?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StopListCell") as! StopListCell
       
       
        if stops?[indexPath.row].rating == 0 {
            cell.firstRatingImageView.image = UIImage(named: "Star Icon-1")
            cell.secondRatingImageView.image = UIImage(named: "Star Icon-1")
            cell.thirdRatingImageView.image = UIImage(named: "Star Icon-1")
            cell.fouthRatingImageView.image = UIImage(named: "Star Icon-1")
            cell.fifthRatingImageView.image = UIImage(named: "Star Icon-1")
        } else if stops?[indexPath.row].rating == 1 {
            cell.firstRatingImageView.image = UIImage(named: "Star Icon")
            cell.secondRatingImageView.image = UIImage(named: "Star Icon-1")
            cell.thirdRatingImageView.image = UIImage(named: "Star Icon-1")
            cell.fouthRatingImageView.image = UIImage(named: "Star Icon-1")
            cell.fifthRatingImageView.image = UIImage(named: "Star Icon-1")
        } else if stops?[indexPath.row].rating == 2 {
            cell.firstRatingImageView.image = UIImage(named: "Star Icon")
            cell.secondRatingImageView.image = UIImage(named: "Star Icon")
            cell.thirdRatingImageView.image = UIImage(named: "Star Icon-1")
            cell.fouthRatingImageView.image = UIImage(named: "Star Icon-1")
            cell.fifthRatingImageView.image = UIImage(named: "Star Icon-1")
        } else if stops?[indexPath.row].rating == 3 {
            cell.firstRatingImageView.image = UIImage(named: "Star Icon")
            cell.secondRatingImageView.image = UIImage(named: "Star Icon")
            cell.thirdRatingImageView.image = UIImage(named: "Star Icon")
            cell.fouthRatingImageView.image = UIImage(named: "Star Icon-1")
            cell.fifthRatingImageView.image = UIImage(named: "Star Icon-1")
        } else if stops?[indexPath.row].rating == 4 {
            cell.firstRatingImageView.image = UIImage(named: "Star Icon")
            cell.secondRatingImageView.image = UIImage(named: "Star Icon")
            cell.thirdRatingImageView.image = UIImage(named: "Star Icon")
            cell.fouthRatingImageView.image = UIImage(named: "Star Icon")
            cell.fifthRatingImageView.image = UIImage(named: "Star Icon-1")
        } else if stops?[indexPath.row].rating == 5 {
            cell.firstRatingImageView.image = UIImage(named: "Star Icon")
            cell.secondRatingImageView.image = UIImage(named: "Star Icon")
            cell.thirdRatingImageView.image = UIImage(named: "Star Icon")
            cell.fouthRatingImageView.image = UIImage(named: "Star Icon")
            cell.fifthRatingImageView.image = UIImage(named: "Star Icon")
        }
      
        cell.nameStopLabel.text = stops?[indexPath.row].name
        cell.discriptionLabel.text = stops?[indexPath.row].desc
        
        if stops?[indexPath.row].spentMoneyCurrencyString == Currency.euro.rawValue {
            cell.spentMoneyLabel.text = "€\(stops?[indexPath.row].spentMoney ?? 0)"
        } else if stops?[indexPath.row].spentMoneyCurrencyString == Currency.dollar.rawValue {
            cell.spentMoneyLabel.text = "$\(stops?[indexPath.row].spentMoney ?? 0)"
        } else if stops?[indexPath.row].spentMoneyCurrencyString == Currency.ruble.rawValue {
            cell.spentMoneyLabel.text = "₽\(stops?[indexPath.row].spentMoney ?? 0 )"
        }

        
        if stops?[indexPath.row].transport == .car {
            cell.transportImageView.image = UIImage(named: "car-directions")
        } else if stops?[indexPath.row].transport == .plane {
            cell.transportImageView.image = UIImage(named: "Aeroplane Mode Icon")
        } else if stops?[indexPath.row].transport == .train {
            cell.transportImageView.image = UIImage(named: "train-on-railroad")
        }
 
        return cell
    }
    
    @objc func addBarButtonClicked(_ sender: UIButton) {
        let createStopVC = CreateStopViewController()
        createStopVC.travel = self.travel
        navigationController?.pushViewController(createStopVC, animated: true)
        
    }
    
    @objc func backToTravelListController(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
