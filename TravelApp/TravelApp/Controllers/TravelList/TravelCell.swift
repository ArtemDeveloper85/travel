//
//  TravelCell.swift
//  TravelApp
//
//  Created by User on 23.02.21.
//

import UIKit

class TravelCell: UITableViewCell {

    @IBOutlet weak var travelNameLabel: UILabel!
    @IBOutlet weak var travelDiscriptionLabel: UILabel!
  
    @IBOutlet weak var firstRatingImageView: UIImageView!
    
    @IBOutlet weak var secondRatingImageView: UIImageView!
    
    @IBOutlet weak var thirdRatingImageView: UIImageView!
    
    @IBOutlet weak var fouthRatingImageView: UIImageView!
    
    @IBOutlet weak var fifthRatingImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    
    
}
