//
//  TravelListViewController.swift
//  TravelApp
//
//  Created by User on 9.02.21.
//

import UIKit
import Firebase
import FirebaseDatabase
import RealmSwift

class TravelListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var labelSearchBar: UILabel!
    
    var travels: Results<RealmTravel>!
    var travelNotificationToken: NotificationToken?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let realm = try! Realm()

        travels = realm.objects(RealmTravel.self)
        travelNotificationToken = travels.observe { [weak self](changes) in
            switch changes {
            
            case .initial(_):
                break
            case .update(_,  let deletions,  let insertions,  let modifications):
                print("deletions: \(deletions), insertions:\(insertions),modifications: \(modifications)  ")
                guard let self = self  else { return }
                self.tableView.reloadData()
                break
            case .error(_):
                break
            }
            
        }
       
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
        let xib = UINib(nibName: "TravelCell", bundle: nil)
        tableView.register(xib, forCellReuseIdentifier: "TravelCell")
        tableView.tableFooterView = UIView()
       
        downloadParseTravels {
            for travel in self.travels {
                self.downloadStops(for: travel) {
                    
                }
            }
        }
    }

        override func viewWillAppear(_ animated: Bool) {
            
            super.viewWillAppear(animated)
           
            
            navigationController?.isNavigationBarHidden = false
            
            navigationItem.title = "Travels"
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addNavigatorButtonClicked(_:)))
            navigationItem.rightBarButtonItem?.tintColor = UIColor(named: "ColorForBar")
            navigationItem.hidesBackButton = true
            
        }
        
    func downloadStops(for travel: RealmTravel, onComplete: (() -> ())? ) {
        ServerManager.shared.downloadTravels(for: travel.id) { (arrayOfDictionaries) in
            DatabaseManager.shared.saveStops(from: arrayOfDictionaries)
            onComplete?()
        }
    }
    
    
    func downloadParseTravels(onComplete: (() -> ())? ) {
        let userId = Auth.auth().currentUser?.uid ?? ""
        ServerManager.shared.downloadTravels(for: userId) {(arrayOfDictionaries) in
            DatabaseManager.shared.saveTravel(from: arrayOfDictionaries)
            onComplete?()
        }
    }

    @objc func addNavigatorButtonClicked(_ sender: UIButton)  {
        
        let alertVC = UIAlertController(title: "To add a new travel", message: "", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "ok", style: .default) { (action) in
            if let userId = Auth.auth().currentUser?.uid {
                let travel = RealmTravel()
                travel.id = UUID().uuidString
                travel.userId = userId
                travel.name = alertVC.textFields![0].text!
                travel.desc = alertVC.textFields![1].text!
                let realm = try! Realm()
                try! realm.write {
                    realm.add(travel)
                }
                
                let json = ["id": travel.id,"userId": userId, "name": travel.name, "description" :travel.description]
                let dataBase = Database.database().reference()
                let child = dataBase.child("travels").child("\(travel.id)")
                child.setValue(json) { (error, ref) in
                    
                }
            }
        }
        
        alertVC.addAction(okAction)
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            print("cancel clicked")
        }
        
        alertVC.addAction(cancelAction)
        
        alertVC.addTextField { (textField) in
            textField.placeholder = "Write title of the travel"
            textField.textColor = .blue
            
        }
        
        alertVC.addTextField { (textField) in
            textField.placeholder = "Write description of the travel..."
            textField.textColor = .red
            
        }
        
        present(alertVC, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return travels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TravelCell") as! TravelCell
        cell.travelNameLabel.text = travels[indexPath.row].name
        cell.travelDiscriptionLabel.text = travels[indexPath.row].desc
        
            if travels[indexPath.row].rating == 0 {
            cell.firstRatingImageView.image = UIImage(named: "Star Icon-1")
            cell.secondRatingImageView.image = UIImage(named: "Star Icon-1")
            cell.thirdRatingImageView.image = UIImage(named: "Star Icon-1")
            cell.fouthRatingImageView.image = UIImage(named: "Star Icon-1")
            cell.fifthRatingImageView.image = UIImage(named: "Star Icon-1")
            } else if travels[indexPath.row].rating == 1 {
            cell.firstRatingImageView.image = UIImage(named: "Star Icon")
            cell.secondRatingImageView.image = UIImage(named: "Star Icon-1")
            cell.thirdRatingImageView.image = UIImage(named: "Star Icon-1")
            cell.fouthRatingImageView.image = UIImage(named: "Star Icon-1")
            cell.fifthRatingImageView.image = UIImage(named: "Star Icon-1")
            } else if travels[indexPath.row].rating == 2 {
            cell.firstRatingImageView.image = UIImage(named: "Star Icon")
            cell.secondRatingImageView.image = UIImage(named: "Star Icon")
            cell.thirdRatingImageView.image = UIImage(named: "Star Icon-1")
            cell.fouthRatingImageView.image = UIImage(named: "Star Icon-1")
            cell.fifthRatingImageView.image = UIImage(named: "Star Icon-1")
            } else if travels[indexPath.row].rating == 3 {
            cell.firstRatingImageView.image = UIImage(named: "Star Icon")
            cell.secondRatingImageView.image = UIImage(named: "Star Icon")
            cell.thirdRatingImageView.image = UIImage(named: "Star Icon")
            cell.fouthRatingImageView.image = UIImage(named: "Star Icon-1")
            cell.fifthRatingImageView.image = UIImage(named: "Star Icon-1")
            } else if travels[indexPath.row].rating == 4 {
            cell.firstRatingImageView.image = UIImage(named: "Star Icon")
            cell.secondRatingImageView.image = UIImage(named: "Star Icon")
            cell.thirdRatingImageView.image = UIImage(named: "Star Icon")
            cell.fouthRatingImageView.image = UIImage(named: "Star Icon")
            cell.fifthRatingImageView.image = UIImage(named: "Star Icon-1")
            } else if travels[indexPath.row].rating == 5 {
            cell.firstRatingImageView.image = UIImage(named: "Star Icon")
            cell.secondRatingImageView.image = UIImage(named: "Star Icon")
            cell.thirdRatingImageView.image = UIImage(named: "Star Icon")
            cell.fouthRatingImageView.image = UIImage(named: "Star Icon")
            cell.fifthRatingImageView.image = UIImage(named: "Star Icon")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let stopVC = StopListViewController()
        stopVC.travel = travels[indexPath.row]
        navigationController?.pushViewController(stopVC, animated: true)
    }
    
}


