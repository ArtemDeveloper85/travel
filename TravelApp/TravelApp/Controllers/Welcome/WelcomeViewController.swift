//
//  WelcomeViewController.swift
//  TravelApp
//
//  Created by User on 9.02.21.
//

import UIKit
import Firebase
class WelcomeViewController: UIViewController {

    
    @IBOutlet weak var createButton: UIButton!
    
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()


    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        navigationItem.backBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: nil)
    }
    
   
    
    @IBAction func createAccountClicked(_ sender: UIButton) {
        let registrationVC = RegistrationViewController()
        navigationController?.pushViewController(registrationVC, animated: true)
        
    }
    
    @IBAction func loginWithEmailClicked(_ sender: UIButton) {
        let loginVC = LoginViewController()
        navigationController?.pushViewController(loginVC, animated: true)
    }
    
}
