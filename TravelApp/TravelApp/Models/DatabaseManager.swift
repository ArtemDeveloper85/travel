//
//  DatabaseManager.swift
//  TravelApp
//
//  Created by User on 27.04.21.
//

import RealmSwift

class DatabaseManager {
    
    static let shared = DatabaseManager()
    
    func saveTravel(from array: [Any]) {
        for item in array {
            if let travelJson = item as? [String: Any] {
                if let id = travelJson["id"] as? String,
                   let name = travelJson["name"] as? String,
                   let description = travelJson["description"] as? String,
                   let userId = travelJson["userId"] as? String {
                    
                    let travel = RealmTravel()
                    travel.id = id
                    travel.userId = userId
                    travel.name = name
                    travel.desc = description
                    let realm = try! Realm()
                    try! realm.write {
                        realm.add(travel, update: .all)
                    }
                }
            }
        }
    }
    
    func saveStops(from array: [Any]) {
        for item in array {
            if let stopJson = item as? [String: Any],
               let id = stopJson["id"] as? String,
               let travelId = stopJson["travelId"] as? String,
               let name = stopJson["name"] as? String,
               let rating = stopJson["rating"] as? Int,
               let spentMoney = stopJson["spentMoney"] as? Double,
               let latitude = stopJson["latitude"] as? Double,
               let longitude = stopJson["longitude"]  as? Double,
               let transport = stopJson["transport"] as? Int,
               let currency = stopJson["currency"] as? String,
               let description = stopJson["description"] as? String {
                let stop = RealmStop()
                stop.id = id
                stop.travelId = travelId
                stop.name = name
                stop.rating = rating
                stop.spentMoney = spentMoney
                stop.desc = description
                stop.transport = Transport(rawValue: transport)!
                stop.spentMoneyCurrencyString = currency
                stop.latitude = latitude
                stop.longitude = longitude
                let realm = try! Realm()
                try! realm.write {
                    realm.add(stop, update: .all)
                }
            }
        }
    }
}
