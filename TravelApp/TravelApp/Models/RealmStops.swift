//
//  RealmStops.swift
//  TravelApp
//
//  Created by User on 27.04.21.
//

import RealmSwift

class RealmStop: Object {
    @objc dynamic var id                       = ""
    @objc dynamic var travelId                 = ""
    @objc dynamic var name                     = ""
    @objc dynamic var rating                   = 1
    @objc dynamic var spentMoney               = 0.0
    @objc dynamic var spentMoneyCurrencyString = ""
    @objc dynamic  var desc                    = ""
    @objc dynamic var transport: Transport     = .car
    @objc dynamic var latitude                 = 0.0
    @objc dynamic var longitude                = 0.0
    
    override class func primaryKey() -> String {
        return #keyPath(RealmStop.id)
    }
}
