//
//  RealmTravel.swift
//  TravelApp
//
//  Created by User on 10.04.21.
//

import RealmSwift

class RealmTravel: Object {
    @objc dynamic   var id   = ""
    @objc dynamic var userId = ""
    @objc dynamic var name   = ""
    @objc dynamic  var desc  = "" 
    let stops = List<RealmStop>()
    var rating: Int {
        var sum = 0
        if stops.isEmpty {
            let count = 1
            return sum / count
        } else  {
            for stop in stops {
                sum += stop.rating
            }
            return sum / stops.count
        }
        
    }
    
        
      
    override class func ignoredProperties() -> [String] {
        return ["rating"]
    }
        override class func primaryKey() -> String {
        return #keyPath(RealmTravel.id)
    }
}



