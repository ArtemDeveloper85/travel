//
//  ServerManager.swift
//  TravelApp
//
//  Created by User on 27.04.21.
//

import Foundation
import FirebaseDatabase

class ServerManager {
    
    static let shared = ServerManager()
    
    func downloadTravels(for userId: String, onComplete: (([Any]) -> ())?) {
        let dataBase = Database.database().reference()
        dataBase
            .child("travels")
            .queryOrdered(byChild: "userId").queryEqual(toValue: userId)
            .observeSingleEvent(of: .value) { response in
                
                if let value = response.value as? [String: Any] {
                    
                    onComplete?(Array(value.values))
                }
            }
    }
    
    
    func downloadStops(for travelId: String, onComplete: (([Any]) -> ())?) {
        let database = Database.database().reference()
        database.child("stops").queryOrdered(byChild: "travelId").queryEqual(toValue: travelId).observeSingleEvent(of: .value) { (response) in
            if let value = response.value as? [String: Any] {
                onComplete?(Array(value.values))
                
            }
        }
    }
}
