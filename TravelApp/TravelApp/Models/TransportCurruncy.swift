//
//  Stop.swift
//  TravelApp
//
//  Created by User on 16.02.21.
//

import Foundation
import RealmSwift

@objc enum Transport: Int, RealmEnum {
    case plane = 0
    case car   = 1
    case train = 2
}

 enum Currency:  String {
    case ruble = "₽", dollar = "$", euro = "€"
}
